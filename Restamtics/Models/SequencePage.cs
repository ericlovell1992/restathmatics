﻿using Optional;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Restamtics.Models
{
    public class SequencePage<T>
    {
        public IEnumerable<T> Sequence { get; set; }
        public Option<T> Min { get; set; }
        public Option<T> Max { get; set; }
    }
}