﻿using Optional;
using Optional.Unsafe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace Restamtics.Models
{
    public class AbstractFunctionalEnumerator<T> : IEnumerator<T>
    {
        private T _current;
        private Func<T, Option<T>> _iteratingFunction;

        public AbstractFunctionalEnumerator(Option<T> start
            , Func<T, Option<T>> iteratingFunction)
        {
            start.Match(i => _current = i
            , () => default(T));
            _iteratingFunction = iteratingFunction
                ?? (t => t.None<T>());
        }

        public T Current => _current;

        object IEnumerator.Current => _current;


        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            var next = _iteratingFunction(_current);
            if (next.HasValue)
            {
                _current = next.ValueOrFailure();
                return true;
            }
            return false;
        }

        public void Reset()
        {
            _current = default(T);
        }
    }
}