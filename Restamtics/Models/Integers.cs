﻿using Optional;
using Optional.Unsafe;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Restamtics.Models
{
    public class Int32s : IEnumerable<int>
    {
        private int _seedInitial;
        private int _seedMax;
        public Int32s(int initial
            , uint maxCount)
        {
            _seedInitial = initial - 1;
            _seedMax = (initial + maxCount > Convert.ToInt64(int.MaxValue))
                ? int.MaxValue
                : Convert.ToInt32(initial + maxCount);
        }
        public IEnumerator<int> GetEnumerator()
            => GetEnumerator(_seedInitial);

        public IEnumerator<int> GetEnumerator(int min)
            => new AbstractFunctionalEnumerator<int>(min.Some()
                , i =>
                {
                    if (i + 1 == _seedMax)
                        return Option.None<int>();
                    return (i + 1).Some();
                });

        IEnumerator IEnumerable.GetEnumerator()
            => GetEnumerator(_seedInitial);
    }

    public class Evens : IEnumerable<int>
    {
        private int _seedInitial;
        private int _seedMax;
        public Evens(int min
            , uint maxCount)
        {

            _seedInitial = min == int.MaxValue
                ? int.MaxValue
                : min % 2 == 1
                    ? min + 1
                    : min;

            var minIndex = int.MinValue/ 2;
            //Note that a remainder of one is ingored
            var maxIndex = int.MaxValue / 2;

            var seedInitialEvenIndex = _seedInitial / 2;

            var seedMaxEvenIndex = Convert.ToInt32(seedInitialEvenIndex == maxIndex
                ? maxIndex
                : seedInitialEvenIndex + maxCount - 1);

            _seedMax = seedMaxEvenIndex * 2;
        }
        public IEnumerator<int> GetEnumerator()
            => GetEnumerator(_seedInitial);

        public IEnumerator<int> GetEnumerator(int min)
            => new AbstractFunctionalEnumerator<int>(min.Some()
                , i =>
                {
                    if (i + 2 == _seedMax || i % 2 != 0)
                        return Option.None<int>();
                    return (i + 2).Some();
                });
        IEnumerator IEnumerable.GetEnumerator()
            => GetEnumerator();
    }
}