﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Restamtics.RequestModels
{
    public class MinMaxCountRequest
    {
        public int Min { get; set; }
        public uint MaxCount { get; set; }
    }

    public class MinMaxCountRequestLong
    {
        public long Min { get; set; }
        public ulong MaxCount { get; set; }
    }
}