﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Restamtics.Models;
using Restamtics.RequestModels;
using Restamtics.Services;
using Optional.Unsafe;
using Optional;
using System.Web.Http.Description;

namespace Restamtics.Controllers
{
    public class IntegersController : ApiController
    {
        [Route("{v1}/integers")]
        [HttpGet]
        [ResponseType(typeof(SequencePage<int>))]
        public IHttpActionResult GetRange([FromUri]string min = "0", [FromUri]string maxCount = "10")
        {
            var intOption = MinMaxCountRequestParser.ParseInt(min, maxCount);
            if (intOption.HasValue)
            {
                var request = intOption.ValueOrFailure();
                var requestedIntegers = request.MaxCount > 0
                    ? (IEnumerable<int>)new Int32s(request.Min, request.MaxCount)
                    : new int[0];
                var response = new SequencePage<int>
                {
                    Sequence = requestedIntegers,
                    Min = requestedIntegers.Any()
                        ? requestedIntegers.First().Some()
                        : Option.None<int>(),
                    Max = requestedIntegers.Any()
                        ? requestedIntegers.Last().Some()
                        : Option.None<int>()
                };
                return Ok(response);
            }
            return CouldNotParseRequestResponse();
        }

        [Route("{v1}/integers/evens")]
        [HttpGet]
        [ResponseType(typeof(SequencePage<int>))]
        public IHttpActionResult GetEvenRange([FromUri]string min = "0", [FromUri]string maxCount = "10")
        {
            var intOption = MinMaxCountRequestParser.ParseInt(min, maxCount);
            if(intOption.HasValue)
            {
                var request = intOption.ValueOrFailure();
                var requestEvens = request.MaxCount > 0
                    ? (IEnumerable<int>)new Evens(request.Min, request.MaxCount)
                    : new int[0];
                var response = new SequencePage<int>
                {
                    Sequence = requestEvens,
                    Min = requestEvens.Any()
                        ? requestEvens.First().Some()
                        : Option.None<int>(),
                    Max = requestEvens.Any()
                        ? requestEvens.Last().Some()
                        : Option.None<int>()
                };
                return Ok(response);
            }
            return CouldNotParseRequestResponse();
        }
        
        private IHttpActionResult CouldNotParseRequestResponse()
        {
            return Content(HttpStatusCode.BadRequest, new {message = "I received your request, but could not map the provided integers to an integer type."});
        }
    }
}
