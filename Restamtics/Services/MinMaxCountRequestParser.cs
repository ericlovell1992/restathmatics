﻿using Optional;
using Restamtics.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restamtics.Services
{
    public static class MinMaxCountRequestParser
    {
        public static Option<MinMaxCountRequest> ParseInt(string min, string maxCount)
        {
            int minInt;
            uint maxCountInt;
            if (int.TryParse(min, out minInt)
                && uint.TryParse(maxCount, out maxCountInt))
            {
                var request = new MinMaxCountRequest { Min = minInt, MaxCount = maxCountInt };
                return Option.Some(request);
            }
            return Option.None<MinMaxCountRequest>();
        }

        public static Option<MinMaxCountRequestLong> ParseLong(string min, string maxCount)
        {
            long minLong;
            ulong maxCountLong;
            if (long.TryParse(min, out minLong)
                && ulong.TryParse(maxCount, out maxCountLong))
            {
                var request = new MinMaxCountRequestLong { Min = minLong, MaxCount = maxCountLong };
                return Option.Some(request);
            }
            return Option.None<MinMaxCountRequestLong>();
        }
    }
}